from django.contrib import admin
from .models import RealtyCategory, RealtyObject, Picture
from django.utils.translation import ugettext_lazy as _

class PictureInline(admin.TabularInline):
    model = Picture
    fk_name = 'related_obj'
    fields = ('title', 'active', 'image')


class RealtyCategoryAdmin(admin.ModelAdmin):
    list_display = ('title', 'active', 'sort')
    inlines = (PictureInline,)
 
admin.site.register(RealtyCategory, RealtyCategoryAdmin)


class PictureAdmin(admin.ModelAdmin):
    list_display = ('title', 'get_model_name', 'active', 'sort')
    fields = ('title', 'active', 'image', 'sort', 'description', )

    def get_model_name(self, obj):
        return getattr(getattr(obj, 'related_obj', None), 'title', None) or ''
    get_model_name.short_description = _('Связанный объект')

admin.site.register(Picture, PictureAdmin)


class RealtyAdmin(admin.ModelAdmin):
    list_display = ('title', 'active', 'sort')
    inlines = (PictureInline,)
    exclude = ('pictures', )

admin.site.register(RealtyObject,RealtyAdmin)
# Register your models here.