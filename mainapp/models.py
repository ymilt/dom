from django.db import models
from django.utils.translation import ugettext_lazy as _

# warenkorb
# Create your models here.
class Core(models.Model):
    title = models.CharField(_('dc-title'), max_length=250, default='', blank=True, null=False)
    description = models.TextField(_('dc-description'), null=True, blank=True)
    sort = models.IntegerField(_(u'sort'), default=0, blank=True, null=True)
    active = models.BooleanField(_(u'active'), default=False)

    def __str__(self):
        return f'{self.title}'

class Picture(Core):
    """docstring for ProductCategory"""
    class Meta:
        verbose_name = _('Картинка')
        verbose_name_plural = _('Картинки')

    image=models.ImageField(upload_to='images')
    related_obj=models.ForeignKey(Core, verbose_name=_(u'pictures'), null=True, blank=True, related_name='pictures', on_delete=models.SET_NULL)

class RealtyCategory(Core):
    """docstring for RealtyCategory"""
    class Meta:
        verbose_name = _('Категория недвижимости')
        verbose_name_plural = _('Категории недвижимости')

class CompanyGroup(Core):
    """docstring for CompanyGroup"""

    class Meta:
        verbose_name = _('Категория Владельцев')
        verbose_name_plural = _('Категория Владельцев')

class OwnerInfo(Core):
    """docstring for ProductCategory"""
    class Meta:
        verbose_name = _('Контакт')
        verbose_name_plural = _('Контакты')

    phone = models.TextField(_('Телефон'), null=True, blank=True)    
    address = models.TextField(_('Адрес'), null=True, blank=True)
    email = models.EmailField(_('Емайл'), null=True, blank=True)
    owner = models.ForeignKey(CompanyGroup, verbose_name=_(u'Владелец'), null=True, blank=True, related_name='contacts', on_delete=models.SET_NULL)

class RealtyObject(Core):
    """docstring for PRealtyObject"""
    class Meta:
        verbose_name = _('Объект')
        verbose_name_plural = _('Объекты')

    category = models.ForeignKey(RealtyCategory, null=True, blank=True, on_delete=models.SET_NULL)
    owner = models.ForeignKey(OwnerInfo, verbose_name=_(u'Владелец'), null=True, blank=True, related_name='contacts', on_delete=models.SET_NULL)
    hottub = models.IntegerField(_(u'hottub'), default=0, blank=True, null=True)
    bedroom = models.IntegerField(_(u'bedroom'), default=0, blank=True, null=True)
    square_meters = models.IntegerField(_(u'meters'), default=0, blank=True, null=True)