import re
i = 0
work_files = ['index.html','about.html','base_template.html','contact.html','listings.html', 
'listings_single.html', 'news.html', 'includes/about_agent.html', 'includes/about_milestone.html',
'includes/archives_mod.html','includes/categories_mod.html','includes/city_item.html','includes/drop_menu.html',
'includes/dropdown_item5.html','includes/dropdown_item6.html','includes/featured_card.html','includes/inc-menu.html','includes/instagram_mod.html',
'includes/latest_mod.html','includes/listings_item.html','includes/news_post.html','includes/search_card.html',
'includes/search_feature_item.html','includes/slider_item.html','includes/testimonial_item.html','includes/workflow_item.html',]
def dashrepl(matchobj):
    global i
    i+=1
    replace_line = ">{{% trans '{}' %}}<".format(matchobj.group(1))
    print(i,matchobj.group(1),replace_line)
    return replace_line

for each in work_files:
    with open (each, 'r' ) as f:
        content = f.read()
    content_new = re.sub('>([^{}<>()]+\S)<', dashrepl, content, flags = re.M)
    f.close()
    with open (each, 'w' ) as g:
        g.write(content_new)
    g.close()