from django.shortcuts import render
from .models import RealtyCategory,  RealtyObject

# Create your views here.
def add_menu(context):
    context['menu'] = [
        {'href': '/', 'name': 'дом'},
        {'href': '/about/', 'name': 'about'},
        {'href': '/contact/', 'name': 'contact'},
        {'href': '/listings/', 'name': 'catalogue'},
        {'href': '/news/', 'name': 'news'},
    ]

def main(request):
    title = 'Home'
    context = {'caption' : title}
    add_menu(context)
    return render(request, 'mainapp/index.html', context)
def about(request):
    title = 'about'
    context = {'caption' : title}
    add_menu(context)
    return render(request, 'mainapp/about.html', context)
def contact(request):
    title = 'contact'
    context = {'caption' : title}
    add_menu(context)
    return render(request, 'mainapp/contact.html', context)
def single(request):
    return render(request, 'mainapp/listings_single.html')
def listings(request):
    title = 'catalogue'
    realty = RealtyObject.objects.all()[:4]

    context = {'caption' : title,  'realty': realty}
    add_menu(context)
    return render(request, 'mainapp/listings.html', context)
def news(request):
    title = 'news'
    context = {'caption' : title}
    add_menu(context)
    return render(request, 'mainapp/news.html', context)
# 